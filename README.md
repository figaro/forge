Forge
=====

Another tool to manage PostgreSQL migrations.

Overview
--------

Forge is a consistent and easy way to apply changes to a PostgreSQL database.
A change is defined in a single files called _step_ these steps get merged
into a single _package_ containing all the transformation and used to
update the DB. The same package can be used to either update a DB on
production or prepare a new DB for testing, providing a simple and
consistent way to apply DB changes.

Init
----

A database managed using _forge_ must be prepared running the command
`forge init --dsn <db dsn>`. This will create a schema `forge` containing the
data used to track all the applied change in the DB.


Steps
-----

A step is an individual change in the database described using pure SQL
and attaching some useful metadata.

Each step is defined in a single file, and its name must be in the following
format `step_{number}_{name}.yaml`. The numbers must be contigous and start
from `0`. The name is a short descriptive text for the change defined by
the step and should be unique to avoid confusion, but it's not required.

A step is defined using YAML and has the following structure:

* `description` (string, default: `""`): text describing the step, useful for describing
the code using human-friendly text and documenting the changes.
* `run` (string, mandatory): the SQL code that will be run in the DB to update the
schema or manipulate the data.
* `transaction` (boolean, default `true`): if `true`, the code will run
within a transaction.

### Example

Step: `step_0_create_table_users.yaml`

```yaml
description: Create a new table for storing all the users.
transaction: true
run: |
    CREATE TABLE users (
        id int primary key,
        email text unique
    )
```

Step: `step_1_add_columns_first_and_last_name.yaml`

```yaml
description: Create a new table for storing all the users.
transaction: true
run: |
    ALTER TABLE users ADD first_name TEXT;
    ALTER TABLE users ADD last_name TEXT;
```

Package
-------

When all steps are ready, _forge_ can build a _package_ which is an
artifact used to describe all changes in a single YAML file. This
file is then used to update the DB, running all changes that have not been
applied yet.

Building a _forge package_ is a reproducible action. It means that using the
same set of steps, it will create the same package, this helps the integration
with some build system like [Bazel](#bazel).

A package is built using the subcommand `build`:
`forge build [-in path] [-out path]`.

### Example

```sh
$ forge build -in examples/step_* -out pkg.yaml
$ cat pkg.yaml
steps:
- name: create_table_users
  description: Create a new table for storing all the users.
  run: |
    CREATE TABLE users (
        id int primary key,
        email text unique
    )
  transaction: true
- name: add_columns_first_and_last_name
  description: Create a new table for storing all the users.
  run: |
    ALTER TABLE users ADD first_name TEXT;
    ALTER TABLE users ADD last_name TEXT;
  transaction: true
```

Apply Changes
-------------

The package file can be used to populate an empty database or to update
an existing one. Both actions are executed using the same forge subcommand
`run`.

The command takes as input the package build using `build` and looks for all
the steps that have not been applied to the database yet. When a step gets
applied, _forge_ will populate the table `forge.steps` to know which steps
were run and find the ones that should be applied.

### Example

```sh
$ docker run -d -e POSTGRES_PASSWORD=secret -p 5432:5432 postgres
$ forge run -dsn "postgres://postgres:secret@localhost?sslmode=disable" -pkg pkg.yaml
$ psql postgres://postgres:secret@localhost:5432 -c "\d users"
       Table "public.users"
   Column   |  Type   | Modifiers 
------------+---------+-----------
 id         | integer | not null
 email      | text    | 
 first_name | text    | 
 last_name  | text    | 
Indexes:
    "users_pkey" PRIMARY KEY, btree (id)
    "users_email_key" UNIQUE CONSTRAINT, btree (email)
```

Bazel
-----

_Forge_ provides a [Bazel](https://docs.bazel.build/) rule `forge`
that creates a package from a list of step files.

```python
load("@io_fucina_rules_forge//:forge.bzl", "forge")

forge(
    name = "pkg",
    srcs = glob(["*.yaml"]),
)
```

### Attributes

| Name | Type | Default | Description |
| ---: | :--- | ------- | ----------- |
| name | [Name](https://docs.bazel.build/versions/master/build-ref.html#name) | required | A unique name for this target. |
| srcs | [sequence](https://docs.bazel.build/versions/master/skylark/lib/list.html) of [File](https://docs.bazel.build/versions/master/skylark/lib/File.html)s | `[]` | A list of step files used to create the final package. |


Development
-----------

This repository is managed using [Bazel](https://docs.bazel.build/).
