
def _build_impl(ctx):
    pkg = ctx.actions.declare_file(ctx.label.name)

    args = ["build", "-out", pkg.path]

    for src in ctx.files.srcs:
        args.extend(["-in", src.path])

    ctx.actions.run(
        mnemonic = "ForgeBuild",
        inputs = ctx.files.srcs,
        outputs = [pkg],
        arguments = args,
        executable = ctx.executable._forge,
    )

    return [DefaultInfo(
        files = depset([pkg]),
    )]


forge = rule(
    doc = "Build a Forge package with the SQL steps used to manage the DB incrementally.",
    implementation = _build_impl,
    attrs = {
        "srcs": attr.label_list(
            doc = "Sources files for each step used to build the DB.",
            allow_files = [".yaml"],
            mandatory = True,
        ),
        "_forge": attr.label(
            executable = True,
            cfg = "host",
            default = Label("//:forge"),
        ),
    },
)
