package main

import "testing"

func TestUnpackStepNameEmptyStringReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameInvalidExtReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("0_create_table.json")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameExtYmlReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("0_create_table.yml")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameMissingPrefixStepReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("0_create_table.yaml")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameMissingNumberReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("step_to_create_table.yaml")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameWithoutNameReturnInvalidNameError(t *testing.T) {
	n, name, err := unpackStepName("step_1.yaml")

	if err != InvalidNameError {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestUnpackStepNameCorrectNameReturnsNoError(t *testing.T) {
	n, name, err := unpackStepName("step_1_create_table.yaml")

	if n == 1 && name == "create_table" && err != nil {
		t.Errorf("error is not an InvalidNameError: n: %#v name: %#v err: %#v", n, name, err)
	}
}

func TestParseStepOnlyDescAndSQL(t *testing.T) {
	step, err := parseStep(0, "create_table_users", []byte("description: lorem ipsum\nrun: create table users ( id integer primary key )"))

	if err != nil {
		t.Errorf("parsing step failed. error: %#v", err)
	}

	expected := Step{
		number:      0,
		Name:        "create_table_users",
		Description: "lorem ipsum",
		Run:         "create table users ( id integer primary key )",
		Transaction: true,
	}

	if step != expected {
		t.Errorf("result doesn't match the expected step: expected: %#v result: %#v", expected, step)
	}
}
