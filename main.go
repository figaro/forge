package main

import (
	"database/sql"
	"errors"
	"flag"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var (
	InvalidNameError         = errors.New("file name is invalid")
	InvalidStepSeuqenceError = errors.New("invalid sequence of steps")
	InconsistentStepsState   = errors.New("package and db state are not consistent")
)

var log = logrus.New()

type StepContent struct {
	Description string
	Run         string
	Transaction bool
}

type Step struct {
	number      int64
	Name        string
	Description string
	Run         string
	Transaction bool
}

type Package struct {
	Steps []Step
}

type ByNumber []Step

func (s ByNumber) Len() int           { return len(s) }
func (s ByNumber) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s ByNumber) Less(i, j int) bool { return s[i].number < s[j].number }

func parseStep(n int64, name string, data []byte) (Step, error) {
	sc := StepContent{
		Description: "",
		Run:         "",
		Transaction: true,
	}

	err := yaml.UnmarshalStrict([]byte(data), &sc)
	if err != nil {
		return Step{}, err
	}

	step := Step{
		number:      n,
		Name:        name,
		Description: sc.Description,
		Run:         sc.Run,
		Transaction: sc.Transaction,
	}

	return step, nil
}

func connect(dsn string) (*sql.DB, error) {
	connector, err := pq.NewConnector(dsn)
	if err != nil {
		return nil, err
	}

	db := sql.OpenDB(connector)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func unpackStepName(fileName string) (int64, string, error) {
	var n int64
	var name string

	parts := strings.SplitN(fileName, ".", 2)
	if len(parts) != 2 {
		return n, name, InvalidNameError
	}

	if parts[1] != "yaml" {
		return n, name, InvalidNameError
	}

	parts = strings.SplitN(parts[0], "_", 3)
	if len(parts) != 3 {
		return n, name, InvalidNameError
	}

	if parts[0] != "step" {
		return n, name, InvalidNameError
	}

	n, err := strconv.ParseInt(parts[1], 10, 32)
	if err != nil {
		return n, name, InvalidNameError
	}

	name = parts[2]

	return n, name, nil
}

func loadPackage(log *logrus.Entry, inPaths []string) (Package, error) {
	var pkg Package
	steps := []Step{}

	if len(inPaths) == 0 {
		log.Warn("no input paths")
	}

	for _, gPath := range inPaths {
		log = log.WithFields(logrus.Fields{"in": gPath})

		log.Info("expanding path pattern")
		paths, err := filepath.Glob(gPath)
		if err != nil {
			return pkg, err
		}

		if len(paths) == 0 {
			log.Warn("pattern does not match any file")
		}

		for _, inPath := range paths {
			log = log.WithFields(logrus.Fields{"path": inPath})

			f, err := os.Stat(inPath)
			if err != nil {
				return pkg, err
			}

			fileName := f.Name()

			n, name, err := unpackStepName(fileName)
			if err == InvalidNameError {
				log.Warn("skipping file with invalid name")
				continue
			} else if err != nil {
				return pkg, err
			}

			log.Info("loading step from file")
			content, err := ioutil.ReadFile(inPath)
			if err != nil {
				return pkg, err
			}

			step, err := parseStep(n, name, content)
			if err != nil {
				return pkg, err
			}

			steps = append(steps, step)
		}
	}

	// check if step numbers are contigous
	if len(steps) > 0 {
		sort.Sort(ByNumber(steps))

		prevStep := steps[0]

		for _, step := range steps[1:] {
			delta := step.number - prevStep.number

			if delta == 0 {
				return pkg, InvalidStepSeuqenceError
			} else if delta != 1 {
				return pkg, InvalidStepSeuqenceError
			}

			prevStep = step
		}
	}

	pkg.Steps = steps

	return pkg, nil
}

func build(log *logrus.Entry, inPaths []string, outPath string) {
	log = log.WithFields(logrus.Fields{
		"out": outPath,
	})

	log.Info("building package")

	pkg, err := loadPackage(log, inPaths)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot create a package from the input files")
	}

	out, err := yaml.Marshal(pkg)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot encode the package")
	}

	log.Info("writing package")
	f, err := os.Create(outPath)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot create the package file")
	}
	defer f.Close()

	_, err = f.Write(out)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot write package file")
	}
}

func initDb(log *logrus.Entry, dsn string, cfg DbCfg) {
	log = log.WithFields(logrus.Fields{
		"dsn": dsn,
	})

	log.Info("initialising db")

	log.Info("connecting to db")
	db, err := connect(dsn)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot connect to db")
	}

	_, err = db.Exec("begin;")
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot start a transaction")
	}

	if cfg.SupportCreateSchema {
		_, err = db.Exec(`
			CREATE SCHEMA IF NOT EXISTS forge;

			CREATE TABLE IF NOT EXISTS forge.steps (
				number INTEGER NOT NULL PRIMARY KEY CHECK (number >= 0),
				name TEXT NOT NULL CHECK (name <> ''),
				description TEXT NOT NULL DEFAULT '',
				run TEXT NOT NULL,
				transaction BOOLEAN NOT NULL DEFAULT 't',
				timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			);
		`)
		if err != nil {
			log.WithFields(logrus.Fields{"error": err}).Fatal("failed when creating forge schema and its tables")
		}
	} else {
		_, err = db.Exec(`
			CREATE TABLE IF NOT EXISTS _forge_steps (
				number INTEGER NOT NULL PRIMARY KEY CHECK (number >= 0),
				name TEXT NOT NULL CHECK (name <> ''),
				description TEXT NOT NULL DEFAULT '',
				run TEXT NOT NULL,
				transaction BOOLEAN NOT NULL DEFAULT 't',
				timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			);
		`)
		if err != nil {
			log.WithFields(logrus.Fields{"error": err}).Fatal("failed when creating forge tables")
		}
	}

	_, err = db.Exec("commit;")
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot commit the transaction")
	}
}

func lockTable(db *sql.DB) error {
	_, err := db.Exec("LOCK TABLE forge.steps IN ACCESS EXCLUSIVE MODE")

	return err
}

func registerStep(db *sql.DB, step Step, cfg DbCfg) error {
	var err error

	if cfg.SupportCreateSchema {
		_, err = db.Exec(`
			INSERT INTO forge.steps (
				number,
				name,
				description,
				run
			) VALUES (
				$1,
				$2,
				$3,
				$4
			)
		`, step.number, step.Name, step.Description, step.Run)
	} else {
		_, err = db.Exec(`
			INSERT INTO _forge_steps (
				number,
				name,
				description,
				run
			) VALUES (
				$1,
				$2,
				$3,
				$4
			)
		`, step.number, step.Name, step.Description, step.Run)
	}

	return err
}

// Checks if a step has already been applied to the pointed database.
// It returns `true` if it can find an exact match for the given step
// in the same position. If the step `step.number` doesn't exist
// it will return `false`.
// It returns `InconsistentStepsState` as error when a step has been
// registered in the same position but it doesn't match the given one.
func isApplied(db *sql.DB, step Step, cfg DbCfg) (bool, error) {
	var (
		number      int
		name        string
		description string
		run         string
		transaction bool
		row         *sql.Row
	)

	if cfg.SupportCreateSchema {
		row = db.QueryRow("SELECT number, name, description, run, transaction FROM forge.steps WHERE number = $1", step.number)
	} else {
		row = db.QueryRow("SELECT number, name, description, run, transaction FROM _forge_steps WHERE number = $1", step.number)
	}

	err := row.Scan(&number, &name, &description, &run, &transaction)
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	dbStep := Step{
		number:      int64(number),
		Name:        name,
		Description: description,
		Run:         run,
		Transaction: transaction,
	}

	if step == dbStep {
		return true, nil
	}

	return false, InconsistentStepsState
}

func apply(log *logrus.Entry, dsn, pkgPath string, cfg DbCfg) {
	log = log.WithFields(logrus.Fields{
		"dsn": dsn,
		"pkg": pkgPath,
	})

	log.Info("applying changes")

	log.Info("connecting to db")
	db, err := connect(dsn)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot connect to db")
	}

	log.Info("loading package from file")
	content, err := ioutil.ReadFile(pkgPath)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot read package file")
	}

	var pkg Package

	err = yaml.UnmarshalStrict(content, &pkg)
	if err != nil {
		log.WithFields(logrus.Fields{"error": err}).Fatal("cannot decode package file")
	}

	if len(pkg.Steps) == 0 {
		log.Warn("no steps in the package")
	}

	for n, step := range pkg.Steps {
		step.number = int64(n)

		log = log.WithFields(logrus.Fields{"step_n": n, "step_name": step.Name})
		log.Info("applying step")

		if step.Transaction {
			_, err := db.Exec("begin;")
			if err != nil {
				log.WithFields(logrus.Fields{"error": err}).Fatal("cannot start a transaction")
			}
		}

		if cfg.SupportLockTable {
			err = lockTable(db)
			if err != nil {
				log.WithFields(logrus.Fields{"error": err}).Fatal("cannot lock the forge table")
			}
		}

		applied, err := isApplied(db, step, cfg)
		if err != nil {
			log.WithFields(logrus.Fields{"error": err}).Fatal("package cannot be applied")
		}

		if applied {
			log.Info("step already applied")
		} else {
			_, err = db.Exec(step.Run)
			if err != nil {
				log.WithFields(logrus.Fields{"error": err}).Fatal("sql execution failed")
			}

			err = registerStep(db, step, cfg)
			if err != nil {
				log.WithFields(logrus.Fields{"error": err}).Fatal("cannot register step")
			}
		}

		if step.Transaction {
			_, err := db.Exec("commit;")
			if err != nil {
				log.WithFields(logrus.Fields{"error": err}).Fatal("cannot commit the transaction")
			}
		}
	}
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

type DbCfg struct {
	SupportCreateSchema bool
	SupportLockTable    bool
}

var DbConfigs = map[string]DbCfg{
	"pg": DbCfg{
		SupportCreateSchema: true,
		SupportLockTable:    true,
	},
	"roach": DbCfg{
		SupportCreateSchema: false,
		SupportLockTable:    false,
	},
}

func init() {
	log.SetOutput(os.Stdout)
	log.SetLevel(logrus.InfoLevel)
}

func main() {
	buildCmd := flag.NewFlagSet("build", flag.ExitOnError)

	var flagInPaths arrayFlags
	buildCmd.Var(&flagInPaths, "in", "path of the files to include in the final build.")
	flagOutPath := buildCmd.String("out", "./pkg.yaml", "path of the build artifact")

	applyCmd := flag.NewFlagSet("apply", flag.ExitOnError)
	flagApplyDsn := applyCmd.String("dsn", "postgres://localhost:5432/", "Database DSN.")
	flagApplyPath := applyCmd.String("pkg", "./pkg.yaml", "Path of the package to apply")
	flagApplyDb := applyCmd.String("db", "pg", "Database used behind the scenes: PostgreSQL `pg`, CockroachDB `roach`")

	initCmd := flag.NewFlagSet("init", flag.ExitOnError)
	flagInitDsn := initCmd.String("dsn", "postgres://localhost:5432/", "Database DSN.")
	flagInitDb := initCmd.String("db", "pg", "Database used behind the scenes: PostgreSQL `pg`, CockroachDB `roach`")

	if len(os.Args) < 2 {
		log.Error("missing command")
		flag.PrintDefaults()
		os.Exit(1)
	}

	cmd := os.Args[1]
	logger := log.WithFields(logrus.Fields{"cmd": cmd})

	switch cmd {
	case "build":
		buildCmd.Parse(os.Args[2:])
		build(logger, flagInPaths, *flagOutPath)

	case "init":
		initCmd.Parse(os.Args[2:])

		logger := log.WithFields(logrus.Fields{"db": *flagInitDb})

		dbCfg, found := DbConfigs[*flagInitDb]
		if !found {
			log.Fatal("unknonwn database")
		}

		initDb(logger, *flagInitDsn, dbCfg)

	case "apply":
		applyCmd.Parse(os.Args[2:])

		logger := log.WithFields(logrus.Fields{"db": *flagApplyDb})

		dbCfg, found := DbConfigs[*flagApplyDb]
		if !found {
			log.WithFields(logrus.Fields{"db": *flagApplyDb}).Fatal("unknonwn database")
		}

		apply(logger, *flagApplyDsn, *flagApplyPath, dbCfg)

	default:
		logger.Error("unknown command")
		flag.PrintDefaults()
		os.Exit(1)
	}
}
