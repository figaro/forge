#!/usr/bin/env bash
# This command is used by bazel as the workspace_status_command
# to implement build stamping with git information.

set -o errexit
set -o nounset
set -o pipefail

cat <<EOF
STABLE_GIT_COMMIT $(git rev-parse HEAD)
EOF
